# One cent blog

This is repository contains a small template for a Jekyll based blog with minimal responsive CSS. It strives to
be a batteries included approach to use Jekyll with a push to deploy workflow.

## Deploying on S3 behind cloudflare

The including the S3 specific deployment code is available in the [S3 branch](https://github.com/k0nserv/one-cent-blog/tree/S3).
[Read the blog post](https://hugotunius.se/aws/cloudflare/web/2016/01/10/the-one-cent-blog.md-the-one-cent-blog.html) for full instructions
on how to set it up.
